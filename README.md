This is primarily a bug-fix and syntax-updating branch of the FASTER toolbox.

Anyone is welcome to contribute. Released under the same GPL license as the original, which you should have received 
a copy of along with this repository.


